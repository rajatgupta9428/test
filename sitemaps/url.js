const fs = require('fs');
const request = require('request');
const TurndownService = require('turndown');
const urlRegex = require('url-regex'); 
const jsonObj = require('./NodeData.json');
const parentObj = require('../metaData.json');

const parentFunc = (parentObj,parent)=>{
    console.log('parent :' + parent);
    const result = parentObj.filter((obj)=>{

            return parseInt(obj.oldId) === parseInt(parent);
        });
    // console.log(result[0]._id);
    return result[0];
} 

const childFunc = (parentObj,child)=>{
    console.log(child)
    const result = parentObj.filter((obj)=>{
            return parseInt(obj.nid) === parseInt(child);
        });
        // console.log(result[0].title);
        return result[0];
}

var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('sitemaps/myfile.txt')
  });
  
  lineReader.on('line', function (line) {
    var text = Object.create(null);
    fs.readFile(line, "utf-8", function(error, data) {
        text = data;
        //  /documents/doc_cq3o9s0ks8zmskt?name=ddffdddhhh&sectionName=test-document-1.1.1
        // const directory_path =  line.split('/').slice(0, -1).join('/');
        const exp = /\/documents\/wiki\/([0-9])*\/([0-9])*\\/gi;
        const get_matches = text.match(exp);
        if (get_matches && get_matches.length > 0){
            get_matches.forEach(url => {
                const oriUrl = url;
                url = url.split('\\')[0];
                // console.log(url);
                const parent = url.split('/').slice(-2)[0];
                const child = url.split('/').slice(-1)[0];
                console.log(parent + '----' + child);
                // parentObj.forEach(obj => {
                //     if(obj.oldId === parent){
                //         var parent_id = obj._id;
                //         var parent_title = obj.title;


                //     }
                // });
                // parentObj.forEach(obj => {
                //     if(obj.nid === child){
                //         var child_title = obj.title;
                //     }
                // });
                // const new_url = `/documents/${parent_id}?name=${parent_title}&sectionName=${child_title}`;
                // console.log(new_url);

                const parentTitle = parentFunc(parentObj,parent).title.toString().split(' ').join('_');
                const childDocTitle = childFunc(jsonObj,child).title.toString().split(' ').join('_').split('__').join('_');               
                const new_url = `/documents/${parentFunc(parentObj,parent)._id}?name=${parentTitle}&sectionName=${childDocTitle}`;

                text = text.replace(oriUrl,new_url);
                    
                    fs.writeFile(line, text, function(err) {
                     if(err) {
                         return console.log(err);
                     }
                     console.log("The file was saved!");
                 }); 
                
            });
        }

        
        
    });
  });