const fs = require('fs');
const request = require('request');
const TurndownService = require('turndown');
const urlRegex = require('url-regex'); 

 

var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('sitemaps/myfile.txt')
  });
  
  lineReader.on('line', function (line) {
    var text = Object.create(null);
    fs.readFile(line, "utf-8", function(error, data) {
        text = data;
        const directory_path =  line.split('/').slice(0, -1).join('/');
        // const exp = /\s*()(https?:\/\/.+?.png)\1/gi;
        
        const turndownService = new TurndownService();
        const markdown = turndownService.turndown(text);
        filePathMd = line.replace(".html", ".md");
        // console.log(filePathMd);
        fs.writeFile(filePathMd, markdown, function(err) {
            if(err) {
                return console.log(err);
            }
            console.log("The file was saved!");
        }); 
        
        
    });
  });