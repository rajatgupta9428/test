const fs = require('fs');
const request = require('request');
const TurndownService = require('turndown');
const urlRegex = require('url-regex'); 

const download = (url, dest, directory_path)=>{
    const destin = directory_path + '/assets';
    if (!fs.existsSync(destin)){
        fs.mkdirSync(destin);
    }
    /* Create an empty file where we can save data */
    const file = fs.createWriteStream(dest);

    /* Using Promises so that we can use the ASYNC AWAIT syntax */
    return new Promise((resolve, reject) => {
      request({
        /* Here you should specify the exact link to the file you are trying to download */
        uri: url,
      })
          .pipe(file)
          .on('finish', async () => {
            console.log(`The file is finished downloading.`);
            resolve();
          })
          .on('error', (error) => {
            reject(error);
          });
    })
        .catch((error) => {
          console.log(`Something happened: ${error}`);
        });
}

const getmatch = (text => {
    const get_all_match = text.match(urlRegex())?.filter((obj)=>{
        return (obj.includes('v2dev.developer.t-mobile.com:8080')&& obj.endsWith('.png'));
    });
    return get_all_match;
})

// download('https://v2dev.developer.t-mobile.com:8080/sites/default/files/inline/images/account-maintenance_654x333_300percent.png','./test.png').then((res)=>{
//     console.log('inside then');
// }).catch((err)=>{
//     console.log(err);
// });


var lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('sitemaps/myfile.txt')
  });
  
  lineReader.on('line', function (line) {
    var text = Object.create(null);
    fs.readFile(line, "utf-8", function(error, data) {
        text = data;
        const directory_path =  line.split('/').slice(0, -1).join('/');
        // const exp = /\s*()(https?:\/\/.+?.png)\1/gi;
        const get_all_match = text.match(urlRegex())?.filter((obj)=>{
            return (obj.includes('v2dev.developer.t-mobile.com:8080'));
        });
        if(get_all_match && get_all_match.length > 0){
            get_all_match.forEach(url => {
                const uri = url.split('.png').slice(0,-1)+'.png';
                const imagename = uri.split('/').slice(-1)[0];
                const dist = directory_path + '/assets/' + imagename;                
                console.log("uri  :" + uri);
                console.log("dist :" + dist);
                download(uri, dist,directory_path).then((data)=>{
                    const rep = 'assets/' + imagename;
                    text = text.replace(url,rep);
                    
                    fs.writeFile(line, text, function(err) {
                     if(err) {
                         return console.log(err);
                     }
                     console.log("The file was saved!");
                 }); 
                 }).catch(err=>{console.log(err)});
                
             });
        }
        
        
    });
  });