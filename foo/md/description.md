"

### T-Mobile for Business (TFB) documentation and business process flows

\\n\\n

RESTful APIs are used by TFB to facilitate partners building functionality to integrate with T-Mobile systems (products and services) into their own websites or software products.

\\n\\n

Partners can be integrated using the standardized OAuth 2.0\*, secured API gateway that routes calls to the internal and backend layers of the TFB experience. It provides access to the functionalities and enforcement of business rules. Partners can learn more about the APIs, including their business functions, contexual process flows, specifications, and general information about the APIs themselves through this guide.

\\n\\n

### Business functions and their underlying process flows

\\n\\n

\\n

\\n

\\n

[![\"https://v2dev.developer.t-mobile.com:8080/sites/default/files/inline/images/account-maintenance_onhover_654x333_300percent_0.png\"](\"/sites/default/files/account-maintenance-100-1-aedd-8-png%20%283%29_2.png\")](\"/documents/wiki/483/484\")

\\n\\n

\\n

\\n

[Account maintenance](\"/documents/wiki/483/484\")

\\n

\\n\\n

[A service for customers to perform activities which include account lookup, suspend and restore, SIM swap service, and change device.](\"/documents/wiki/483/484\")

\\n

\\n

\\n

\\n\\n

\\n

\\n

![\"https://v2dev.developer.t-mobile.com:8080/sites/default/files/inline/images/customer-consent_onhover_654x333px_300percent.png\"](\"/sites/default/files/customer-consent-654-x-333-px-300-percent-f-67-a-67-png.png\")

\\n\\n

\\n

\\n

Customer consent

\\n

\\n\\n

Provides consent management APIs to capture consent from customers and manage collected user end information.

\\n

\\n

\\n

\\n\\n

\\n

\\n

[![\"https://v2dev.developer.t-mobile.com:8080/sites/default/files/inline/images/ordering_onhover_654x333_300percent.png\"](\"/sites/default/files/ordering-654-x-333-300-percent-5-a-0470-png.png\")](\"/documents/wiki/483/488\")

\\n\\n

\\n

\\n

[Ordering](\"/documents/wiki/483/488\")

\\n

\\n\\n

[Gives customers information about order activity which allows users to activate SIM, add a line, get updates on order details, status, history, etc.](\"/documents/wiki/483/488\")

\\n

\\n

\\n

\\n

\\n\\n

\\n

\\n

\\n

[![\"https://v2dev.developer.t-mobile.com:8080/sites/default/files/inline/images/billing_onhover_654x333_300percent.png\"](\"/sites/default/files/billing-654-x-333-300-percent.png\")](\"/documents/wiki/483/496\")

\\n\\n

\\n

\\n

[Billing](\"/documents/wiki/483/496\")

\\n

\\n\\n

[Catered for customer billing information and allows enrollment to services which include autopay, paperless billing, and more.](\"/documents/wiki/483/496\")

\\n

\\n

\\n

\\n\\n

\\n

\\n

![\"https://v2dev.developer.t-mobile.com:8080/sites/default/files/inline/images/entitlements_onhover_654x333_300percent.png\"](\"/sites/default/files/entitlements-100-9540-c-8-png.png\")

\\n\\n

\\n

\\n

Entitlements

\\n\\n

\\n

\\n\\n

Helps create a customer experience for role management of adminstration and organizational resources.

\\n

\\n

\\n

\\n\\n

\\n

\\n

\\n\\n

\\n

\\n

\\n

\\n\\n

\\n

\\n

\\n

\\n

\\n

\\n

\\n\\n

\* OAuth 2.0 is an industry open standard protocol for authorization used by many companies to provide secure access to protected resources.

\\n\\n

\\n"